package com.example.DemoRestFull;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculadoraControllerTest {

	@Test
	void testAdd() {
		CalculadoraController calcontroller = new CalculadoraController();
		assertEquals(calcontroller.add(10,902),"912");

	}

	@Test
	void testSubstract() {
		CalculadoraController calcontroller = new CalculadoraController();
		assertEquals(calcontroller.substract(10,2),"8");

	}

	@Test
	void testMultiply() {
		CalculadoraController calcontroller = new CalculadoraController();
		assertEquals(calcontroller.multiply(5,5),"25");

	}

	@Test
	void testDivide() {
		CalculadoraController calcontroller = new CalculadoraController();
		assertEquals(calcontroller.divide(15,3),"5");

	}

}
