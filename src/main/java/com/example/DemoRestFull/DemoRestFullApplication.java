package com.example.DemoRestFull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRestFullApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRestFullApplication.class, args);
	}

}
