FROM openjdk:11.0.6-jdk
LABEL usuario="Renzo Jaramillo"
WORKDIR /workspace
COPY target/demo*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar