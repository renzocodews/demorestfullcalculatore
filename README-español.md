
# README
# Aplicacion Web Calculadora

La aplicacion es un prototipo de calculadora web se basa en comandos basicos de una calculadora como sumar, restar, multiplciar y dividar, es un programa desarrollado en JAVA, y despliegue un FAT JAR, el cual se verifica las fases de Maven.


### Herramientas

* Git
* Jenkins
* Jacoco
* J Unit 4
* Sonarqube
* JFrog Artifactory
* Docker Hub
* Eclipse


[DiguitalOcena]
(https://bitbucket.org/renzocodews/demorestfullcalculatore/src/master/imgage/img1.png)

## Comenzando 🚀

Cuenta con servicios para las oepracioens basicas de una calculadora y ademas con un contralador para el paso de mensajes

### Pre-requisitos 📋

* Maven
* Cuenta Bitbucket
* Cuenta Diguital Ocean
* Cuenta Docker Hub


## Ejecutando las pruebas ⚙️

Pruebas unitarias con J Unit 4 y Sonarqube y Jacoco para medir la cobertura.

[Sonarqube]
(https://bitbucket.org/renzocodews/demorestfullcalculatore/src/master/imgage/img2.png)

## Despliegue 📦
Se lo realizo por medio de Docker Hub ya que a los microservicios se los despliegua en contenedores como Docker.
[Dockerhub]
(https://bitbucket.org/renzocodews/demorestfullcalculatore/src/master/imgage/img3.png)

## Versionado 📌

Se lo realizo con Jfrog Artefactory para el versionamiento del artefcto tipo war creado en el desarrollo del al aplicacion.
[JFrogArtifactory]
(https://bitbucket.org/renzocodews/demorestfullcalculatore/src/master/imgage/img4.png)

## Autores ✒️

* **Renzo Jaramillo** - *Trabajo Inicial* - [renzocodews](https://bitbucket.org/renzocodews)


## Licencia 📄

Este proyecto está bajo la Licencia - mira el archivo [LICENSE.md](LICENSE.md) para detalles
